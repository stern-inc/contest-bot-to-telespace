from sqlalchemy import create_engine, Table, Column, Integer, Boolean, MetaData, String


meta = MetaData()

users_ankets = Table('Users_ankets', meta, 
    Column('user_id', Integer, primary_key=True),
    Column('username', Integer, nullable=False),
    Column('firstname', Integer, nullable=False),
    Column('feedback', Integer, nullable=False),
    Column('experience', Integer, nullable=False),
    Column('target', Integer, nullable=False),
    Column('invest_state', Integer, nullable=False),
    Column('invest_size', Integer, nullable=False),
    Column('sources', Integer, nullable=False),
    Column('income', Integer, nullable=False),
    Column('sent', Boolean, nullable=False),
    Column('step', Integer, nullable=False),
    Column('firstname_mid', Integer, nullable=False),
    Column('feedback_mid', Integer, nullable=False)
    )



engine = create_engine('sqlite:///telespace_users.db')
meta.create_all(engine)