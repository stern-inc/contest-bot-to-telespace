from datetime import datetime
from multiprocessing import parent_process
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from sqlalchemy import MetaData, Table, create_engine
from sqlalchemy.orm import mapper, sessionmaker
import anketa

anketa_quests = anketa.anketa_quests
g_chat_id = 15022003
token = 'token'
bot = Bot(token=token)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
engine = create_engine('sqlite:///telespace_users.db')
meta = MetaData(engine)
users_ankets = Table('Users_ankets', meta, autoload=True)


class Users_ankets():
    def __init__(self, user_id, username, firstname, feedback, experience, target, invest_state, invest_size, sources, income, sent, step, firstname_mid,feedback_mid):
        self.user_id = user_id
        self.username = username
        self.firstname = firstname
        self.feedback = feedback
        self.experience = experience
        self.target = target
        self.invest_state = invest_state
        self.invest_size = invest_size
        self.sources = sources
        self.income = income
        self.sent = sent
        self.step = step
        self.firstname_mid = firstname_mid
        self.feedback_mid = feedback_mid

def anketa_text(user):
    return f'Имя — {user.firstname}\nОбратная связь — {user.feedback}\n\nОпыт работы — {user.experience}\
    \nЦель обучения — {user.target}\nВозможность вложений — {user.invest_state}\nБюджет — {user.invest_size}\nИсточник — {user.sources}\nЗаработок — {user.income}'


async def anketa_fill(user):
    if user.step == 0:
        fname = await bot.send_message(user.user_id, 'Как к Вам можно обращатся?')
        user.firstname_mid = fname.message_id; session.commit(); return
    elif user.step == 1:
        feedb = await bot.send_message(user.user_id, 'Оставьте информацию для связи с Вами\nТелефон или email')
        user.feedback_mid = feedb.message_id; session.commit(); return
    elif user.step <= 7:
        ans_kb = types.InlineKeyboardMarkup(row_width=(len(anketa_quests[user.step - 2]) - 1))
        msg1 = ''
        for i in anketa_quests[user.step - 2][1:]:
            msg1 += f'\n{i}\n'
            ans_kb.insert(types.InlineKeyboardButton(f'{i[0:2]}', callback_data=f'{user.user_id}|{i[0]}|next_step'))
        await bot.send_message(user.user_id, f'<b>{anketa_quests[user.step - 2][0]}</b>\n{msg1}', reply_markup=ans_kb, parse_mode="HTML")
        return
    elif user.step == 8:
        kb1 = types.InlineKeyboardMarkup()
        kb1.add(types.InlineKeyboardButton('Отправить анкету', callback_data=f'{user.user_id}|send'))
        kb1.add(types.InlineKeyboardButton('Заполнить повторно', callback_data=f'{user.user_id}|refill'))
        await bot.send_message(user.user_id, f'<b>Проверьте корректность данных</b>\n\n{anketa_text(user)}',\
                parse_mode="HTML", reply_markup=kb1)

def get_data(user, number):
    for i in anketa_quests[user.step - 2][1:]:
        if int(i[0]) == number:
            return i[5:]


@dp.callback_query_handler()
async def give_react(call: types.CallbackQuery): 
    async def go_step(callback, userr): user.step += 1; session.commit(); await callback.message.delete(); await anketa_fill(userr)
    
    data = call.data.split('|')
    user = session.query(Users_ankets).get(int(data[0]))
    if user == None:
        return
    if 'send' in data[1]:
        sent_kb = types.InlineKeyboardMarkup(row_width=2)
        sent_kb.add(types.InlineKeyboardButton('1⃣ФАКУЛЬТЕТ1⃣', callback_data=f'{user.user_id}|fac_1'))
        sent_kb.insert(types.InlineKeyboardButton('2⃣ФАКУЛЬТЕТ2⃣', callback_data=f'{user.user_id}|fac_2'))
        sent_kb.add(types.InlineKeyboardButton('3⃣ФАКУЛЬТЕТ3⃣', callback_data=f'{user.user_id}|fac_3'))
        sent_kb.insert(types.InlineKeyboardButton('4⃣ФАКУЛЬТЕТ4⃣', callback_data=f'{user.user_id}|fac_4'))
        sent_kb.add(types.InlineKeyboardButton('5⃣ФАКУЛЬТЕТ5⃣', callback_data=f'{user.user_id}|fac_5'))
        sent_kb.insert(types.InlineKeyboardButton('6⃣ФАКУЛЬТЕТ6⃣', callback_data=f'{user.user_id}|fac_6'))
        sent_kb.add(types.InlineKeyboardButton('👑VIP👑', callback_data=f'{user.user_id}|VIP'))
        sent_kb.add(types.InlineKeyboardButton('⚠SPAM⚠', callback_data=f'{user.user_id}|SPAM'))
        await call.message.edit_text('Анкета успешно отпралвена\nОжидайте проверки куратором')
        await bot.send_message(-1001545575401, f'<b>Новая анкета</b>\n{anketa_text(user)}', reply_markup=sent_kb, parse_mode="HTML"); user.sent = True; session.commit()
    elif 'refill' in data[1]:
        user.step = 0; session.commit(); await call.message.delete(); await anketa_fill(user)
    elif 'SPAM' in data[1]: await call.message.delete(); user.sent = False; session.commit()
    elif 'VIP' in data[1]: pass
        
    if len(data) == 3 and 'next_step' in data[2]:
        if user.step == 2: user.experience = get_data(user, int(data[1])); await go_step(call, user)
        elif user.step == 3: user.target = get_data(user, int(data[1])); await go_step(call, user)
        elif user.step == 4: user.invest_state = get_data(user, int(data[1])); await go_step(call, user)
        elif user.step == 5: user.invest_size = get_data(user, int(data[1])); await go_step(call, user)
        elif user.step == 6: user.sources = get_data(user, int(data[1])); await go_step(call, user)
        elif user.step == 7: user.income = get_data(user, int(data[1])); await go_step(call, user)



@dp.message_handler(commands=['start'])
async def start_cmd(message : types.Message):
    if message.from_user.id in [i.user_id for i in session.query(Users_ankets)]:
        pass
    else:
        if message.from_user.username == None: usname = f'firstname|{message.from_user.first_name}'
        else: usname = f'username|{message.from_user.username}'
        new_user = Users_ankets(message.from_user.id, usname, '', 0, 0, 0, 0, 0, 0, 0, False, 0, 0, 0)
        session.add(new_user)
        session.commit()
    #await bot.
    user = session.query(Users_ankets).get(message.from_user.id)
    if user.sent: await message.answer('Ваша анкета уже отправлена на прокерку\nОжидайте проверки куратором'); return
    await anketa_fill(user)

@dp.message_handler(commands=['getid'])
async def start_cmd(message : types.Message):
    await message.answer(message.chat)


@dp.message_handler()
async def other(message: types.Message):
    user = session.query(Users_ankets).get(message.from_user.id)
    if user == None:
        await start_cmd(message)
        return
    if user.step == 0:
        user.firstname = message.text
        user.step += 1
        session.commit()
        await bot.delete_message(user.user_id, message.message_id)
        await bot.delete_message(user.user_id, user.firstname_mid)
        await anketa_fill(user)
        return
    elif user.step == 1:
        user.feedback = message.text
        user.step += 1
        session.commit()
        await bot.delete_message(user.user_id, message.message_id)
        await bot.delete_message(user.user_id, user.feedback_mid)
        await anketa_fill(user)
        return
    else:
        pass
    
mapper(Users_ankets, users_ankets)
DBSession = sessionmaker(bind=engine)
session = DBSession()




if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)